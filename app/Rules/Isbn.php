<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class Isbn implements Rule
{
    const ISBN_DIGITS = 10;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!is_string($value)) return false;
        $valueToArray = str_split($value);
        if (count($valueToArray) != self::ISBN_DIGITS) return false;
        $finalNumber = 0;
        foreach ($valueToArray as $key => $value) {
            $finalNumber += (self::ISBN_DIGITS - (int)$key) * (int)$value;
        }

        return $finalNumber % 11 ? false : true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute is invalid.';
    }
}
