<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Response;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro('success', function (string $message, $data = null) {
            $value = [
                'status' => 'OK',
                'message' => $message,
                'data' => $data
            ];
            return Response::make($value);
        });

        Response::macro('error', function (string $message, array $errors = null, $status = 200) {
            $value = [
                'status' => 'FAILED',
                'message' => $message,
                'data' => $errors
            ];
            return Response::make($value, $status);
        });
    }
}
