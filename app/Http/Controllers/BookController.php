<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Http\Requests\StoreBook;
use App\Http\Resources\BookResource;
use Illuminate\Http\Request;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Book::where('id', '!=', null);
        $request['status'] ? $query->where('status', $request['status']) : $query;

        // filter by date
        $request['date_from'] ? $query->where('created_at',  '>=', $request['date_from']) : $query;
        $request['date_to'] ? $query->where('created_at',  '<=', $request['date_to']) : $query;

        $logs = $query->orderBy('created_at', 'desc')->paginate(request()->input('page_size'));
        return response()->success('Retrieved successfully.', BookResource::collection($logs)->resolve());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBook $request)
    {
        $data = $request->validated();

        $book = Book::create($data);

        $book->refresh();

        return response()->success('Book created successfully', (new BookResource($book))->resolve());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        return response()->success('Retrieved successfully', (new BookResource($book))->resolve());

    }

    /**
     * Check in a book.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function checkIn(Book $book)
    {
        $user = auth()->user();
        if ($book->status !== Book::CHECKED_OUT_STATUS)
            return response()->error('This book has not been checked out.');
        if (!$book->isCheckedOutUser($user->id))
            return response()->error('You didn\'t checkout this book.');
        $book->checkIn($user);
        return response()->success('Check in successful.');
    }

    /**
     * Check in a book.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function checkOut(Book $book)
    {
        if ($book->status !== Book::AVAILABLE_STATUS)
            return response()->error('This book is not available at the moment.');
        $book->checkOut(auth()->user());
        return response()->success('Check out successful.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(StoreBook $request, Book $book)
    {
        $book->update($request->validated());

        return response()->success('Book successfully updated', (new BookResource($book))->resolve());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        $book->delete();

        return response()->success('Record successfully deleted');
    }
}
