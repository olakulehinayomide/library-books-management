<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserActionLogResource;
use App\Models\UserActionLog;
use Illuminate\Http\Request;

class UserActionLogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = UserActionLog::where('id', '!=', null);
        $request['action'] ? $query->where('action', $request['action']) : $query;

        // filter by date
        $request['date_from'] ? $query->where('created_at',  '>=', $request['date_from']) : $query;
        $request['date_to'] ? $query->where('created_at',  '<=', $request['date_to']) : $query;

        $logs = $query->orderBy('created_at', 'desc')->paginate(request()->input('page_size'));
        return response()->success('Retrieved successfully.', UserActionLogResource::collection($logs)->resolve());
    }
}
