<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use App\Http\Requests\StoreSessionRequest;
use App\Http\Requests\StoreUser;
use App\Models\User;

class UserController extends Controller
{

     /**
     * Controller instance
     *
     */
    public function __construct()
    {

        $this->middleware('auth:api', ['only' => ['get', 'destroy']]);
    }

    public function create(StoreUser $request) {

        $data = $request->validated();

        $data = array_merge($data, [
            'password'    => bcrypt($data['password']),
        ]);

        $user = User::create($data);

        return response()->success('User successfully created.', (new UserResource($user))->resolve());
    }

    public function login(StoreSessionRequest $request) {

        $loginData = $request->validated();

        if (!auth()->attempt($loginData)) {
            return response()->error('Invalid Email or Password');
        }

        $user = auth()->user();

        $accessToken = auth()->user()->createToken('authToken')->accessToken;

        return response()->success('Logged in successfully.', array_merge((new UserResource($user))->resolve(), ['token'   => $accessToken ]));
    }

    /**
     * Get details for current logged in customer
     *
     * @return \Illuminate\Http\Response
     */
    public function get()
    {
        $user = auth()->user();

        return response()->success('Retrieved successfully', (new UserResource($user))->resolve());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        auth()->user()->token()->revoke();

        return response()->success('Logged out successfully.');
    }
}
