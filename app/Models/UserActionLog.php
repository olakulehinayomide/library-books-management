<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserActionLog extends Model
{
    use SoftDeletes;

    const CHECKOUT_ACTION    = 'CHECKOUT';
    const CHECKIN_ACTION     = 'CHECKIN';

    const ACTIONS = [
        self::CHECKOUT_ACTION,
        self::CHECKIN_ACTION
    ];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'book_id', 'user_id', 'action'
    ];

    /**
     * Get the book.
     */
    public function book()
    {
        return $this->belongsTo('App\Models\Book');
    }

    /**
     * Get the user.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
