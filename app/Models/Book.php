<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Book extends Model
{
    use SoftDeletes;
    const CHECKED_OUT_STATUS    = 'CHECKED_OUT';
    const AVAILABLE_STATUS      = 'AVAILABLE';

    const STATUSES = [
        self::CHECKED_OUT_STATUS,
        self::AVAILABLE_STATUS
    ];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'isbn', 'published_at', 'status'
    ];

    /**
     * Get the actionLogs.
     */
    public function actionLogs()
    {
        return $this->hasMany('App\Models\UserActionLog');
    }

    /**
     * Get the lastActionLog.
     */
    public function lastActionLog()
    {
        return $this->actionLogs()->oldest()->first();
    }

    /**
     * Check if user is checked out user
     */
    public function isCheckedOutUser($user_id)
    {
        $lastActionUser = optional($this->lastActionLog())->user_id;
        return $lastActionUser == $user_id;
    }

    /**
     * update status
     */
    public function updateStatus($status)
    {
        $this->status = $status;
        $this->update();
    }

    public function checkOut(User $user)
    {
        $this->updateStatus(Book::CHECKED_OUT_STATUS);
        event('book.checkout.after', ['book' => $this, 'user' => $user]);
    }

    public function checkIn(User $user)
    {
        $this->updateStatus(Book::AVAILABLE_STATUS);
        event('book.checkin.after', ['book' => $this, 'user' => $user]);
    }
}
