<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Models\UserActionLog;
use App\Models\Book;
use App\Models\User;

class UserActionLogSubscriber
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the check in event.
     *
     * @return void
     */
    public function handleCheckInLog(Book $book, User $user)
    {
        $createData = [
            'user_id' => $user->id,
            'book_id' => $book->id,
            'action' => UserActionLog::CHECKIN_ACTION
        ];

        $this->createLog($createData);
    }

    /**
     * Handle the checkout event.
     *
     * @return void
     */
    public function handleCheckOutLog(Book $book, User $user)
    {
        $createData = [
            'user_id' => $user->id,
            'book_id' => $book->id,
            'action' => UserActionLog::CHECKOUT_ACTION
        ];

        $this->createLog($createData);
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  \Illuminate\Events\Dispatcher  $events
     * @return void
     */
    public function subscribe($events)
    {
        $events->listen(
            'book.checkin.after',
            [UserActionLogSubscriber::class, 'handleCheckInLog']
        );

        $events->listen(
            'book.checkout.after',
            [UserActionLogSubscriber::class, 'handleCheckOutLog']
        );
    }

    private function createLog($data) {
        UserActionLog::create($data);
    }
}
