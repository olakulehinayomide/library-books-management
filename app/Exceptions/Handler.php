<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Auth\AuthenticationException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        if ($exception instanceof AuthorizationException) {
            return response()->error('Unauthorised.', null, 403);
        }
        if ($exception instanceof AuthenticationException) {
            return response()->error('You need to login to continue.', null, 401);
        }
        if ($exception instanceof ModelNotFoundException) {
            return response()->error('Resource could not be found', null, 404);
        }
        if($exception instanceof \Illuminate\Database\QueryException){
            $errorCode = $exception->errorInfo[1];
            switch ($errorCode) {
                case 1062://code duplicate entry
                    return response()->error('Duplicate entry found', null, 422);
                    break;
                case 1364:// you can handel any auther error
                    if (config('app.debug')) {
                        return response()->error($exception->getMessage(), null, 422);
                    }
                    return response()->error('An unexpected error occured! Please try again later!', null, 422);
                    break;
                default:
                    if (config('app.debug')) {
                        return response()->error($exception->getMessage(), null, 422);
                    }
                    return response()->error('An unexpected error occured! Please try again later!', null, 422);
                    break;
            }
        }
        return parent::render($request, $exception);
    }
}
