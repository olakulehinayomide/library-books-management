<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('auth')->group(function () {
    Route::post('login', 'UserController@login');

    Route::post('register', 'UserController@create');

    Route::get('get', 'UserController@get');

    Route::delete('logout', 'UserController@destroy');
});

Route::apiResource('book', 'BookController');

Route::prefix('book')->group(function () {

    Route::get('/checkin/{book}', 'BookController@checkIn')->middleware('auth:api');

    Route::get('/checkout/{book}', 'BookController@checkOut')->middleware('auth:api');
});

Route::get('/user-action-logs', 'UserActionLogController@index')->middleware('auth:api');
