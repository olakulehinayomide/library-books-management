<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Artisan;

class OauthPersonalAccessClientsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = DB::table('oauth_personal_access_clients')->count();
        if($count === 0) {
            Artisan::call('passport:client --personal -n');
        }
    }
}
