<?php

use Illuminate\Database\Seeder;
use App\Models\Book;
use Illuminate\Support\Facades\DB;

class BookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = DB::table('books')->count();
        if($count === 0) {
            factory(Book::class, 20)
                ->create();
        }
    }
}
