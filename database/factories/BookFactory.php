<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Book;
use Faker\Generator as Faker;

$factory->define(Book::class, function (Faker $faker) {
    return [
        'title' => $faker->realText(10),
        'isbn' => $faker->isbn10,
        'published_at' => $faker->date('Y-m-d', 'now')
    ];
});
