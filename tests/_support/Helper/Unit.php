<?php
namespace Helper;

use App\Models\User;
use App\Models\Book;
// here you can define custom actions
// all public methods declared in helper class will be available in $I

class Unit extends \Codeception\Module
{
    public function createUser($data = [])
    {
        return factory(User::class)->create($data);
    }

    public function createBook($data = [])
    {
        return factory(Book::class)->create($data);
    }
}
