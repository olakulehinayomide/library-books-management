<?php

class RegistrationTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testCanCreateUser()
    {
        $user = $this->tester->createUser();
        $this->tester->assertNotNull($user);
        $this->tester->assertIsObject($user);
    }
}
