<?php

use App\Models\Book;
use App\Models\UserActionLog;

class CheckInAndOutTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected $user;

    protected $availableBook;

    protected $checkedOutBook;

    protected function _before()
    {
        $this->user = $this->tester->createUser();

        $this->availableBook = $this->tester->createBook([
            'status' => Book::AVAILABLE_STATUS
        ]);

        $this->checkedOutBook = $this->tester->createBook([
            'status' => Book::CHECKED_OUT_STATUS
        ]);
    }

    protected function _after()
    {
    }

    // tests
    public function testCheckIn()
    {
        $this->checkedOutBook->checkIn($this->user);

        $this->tester->assertEquals($this->checkedOutBook->status, Book::AVAILABLE_STATUS);

        $lastActionLog = $this->checkedOutBook->lastActionLog();

        $this->assertNotNull($lastActionLog);

        $this->tester->assertEquals($lastActionLog->user_id, $this->user->id);

        $this->tester->assertEquals($lastActionLog->action, UserActionLog::CHECKIN_ACTION);
    }

    // tests
    public function testCheckOut()
    {
        $this->availableBook->checkOut($this->user);

        $this->tester->assertEquals($this->availableBook->status, Book::CHECKED_OUT_STATUS);

        $lastActionLog = $this->availableBook->lastActionLog();

        $this->assertNotNull($lastActionLog);

        $this->tester->assertEquals($lastActionLog->user_id, $this->user->id);

        $this->tester->assertEquals($lastActionLog->action, UserActionLog::CHECKOUT_ACTION);
    }
}
