<?php

use Faker\Factory;

class CreateBookCest
{
    private $faker;

    public function _before(ApiTester $I)
    {
        $this->faker = Factory::create();
    }

    // tests create book api API
    public function testCreateBook(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPost('/api/book', [
            'title' => $this->faker->realText(10),
            'isbn' => '0005534186',
            'published_at' => $this->faker->date('Y-m-d', 'now')
        ]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseContains('"status":"OK"');
    }

    // tests cannot subscribe to the same topic with the same url twice
    public function testInvalidIsbnCreateBook(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPost('/api/book', [
            'title' => $this->faker->realText(10),
            'isbn' => '12344566454',
            'published_at' => $this->faker->date('Y-m-d', 'now')
        ]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseContains('"status":"FAILED"');
    }
}
