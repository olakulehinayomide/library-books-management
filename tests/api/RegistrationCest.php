<?php

use Faker\Factory;

class RegistrationCest
{
    private $faker;

    protected $password = '12345678';

    protected $email;

    public function _before(ApiTester $I)
    {
        $this->faker = Factory::create();

        $this->email = $this->faker->unique()->safeEmail;
    }

    // tests
    public function testRegistrationEndpoint(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPost('/api/auth/register', [
            'name' => $this->faker->name,
            'email' => $this->email,
            'date_of_birth' => $this->faker->date('Y-m-d', 'now'),
            'password' => $this->password,
            'password_confirmation' => $this->password,
        ]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseContains('"status":"OK"');
    }

    // tests
    public function testLoginEndpoint(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPost('/api/auth/login', [
            'email' => $this->email,
            'password' => $this->password,
        ]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseContains('"status":"FAILED"');
    }
}
